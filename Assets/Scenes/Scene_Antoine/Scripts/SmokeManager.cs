﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmokeManager : MonoBehaviour
{
    [Header("PrimaryVariable")]
    public ParticleSystem smoke;
    public GameObject[] spot;
    [Header("SecondaryVariable")]
    public float Delay;

    private void Update()
    {
        //Input for create smoke instance
        if (Input.GetKeyUp(KeyCode.F))
        {
            StartCoroutine(InitSmoke());
        }
    }

    IEnumerator InitSmoke()
    {
        foreach (var item in spot)
        {
            //Storage of the smoke instance into variable ps 
            ParticleSystem ps = Instantiate(smoke, item.transform.position, Quaternion.Euler(item.transform.eulerAngles));
            ps.transform.parent = item.transform;
            //Can use some delay for each smoke instance
            yield return new WaitForSeconds(Delay);
        }
    }
}
