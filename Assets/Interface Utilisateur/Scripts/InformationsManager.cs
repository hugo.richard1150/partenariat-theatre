﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InformationsManager : MonoBehaviour
{
    public Animator anim1;
    public Animator anim2;

    public void ActiveInformation()
    {
        anim1.Play("FadeIn");
        anim2.Play("FadeOut");
    }
}
