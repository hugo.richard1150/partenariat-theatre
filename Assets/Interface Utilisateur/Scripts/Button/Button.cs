﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace AntDev
{
    public class Button : MonoBehaviour
    {    
        [Header("MainElement")]
        public Animator[] anim;
        void Start()
        {
            foreach (var item in anim)
            {
                item.gameObject.SetActive(false);
            }
        }

        public void ActiveScene(GameObject currentButton)
        {
            //each case for scene (1, 2, 3, 4)

            for (int i = 0; i < anim.Length; i++)
            {
                bool isSame = currentButton == anim[i].gameObject;
                anim[i].gameObject.SetActive(isSame);

                if (isSame)
                {
                    anim[i].Play("ShowUp");                    
                }
            }
        }
    }
}
