﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlateManager : MonoBehaviour
{
    [Header("MainVariable")]
    string mainTargetText;
    public GameObject[] plateController;
    char[] character;

    [Header("SecondaryVariable")]
    public float nextTiming;

    public void SetTitle(string targetText)
    {
        StartCoroutine(Refresh());
        mainTargetText = targetText;
        character = mainTargetText.ToCharArray(0, mainTargetText.Length);
        StartCoroutine(Initialize());
    }

    IEnumerator Initialize()
    {
        for (int i = 0; i < plateController.Length; i++)
        {
            foreach (char letter in character)
            {
                plateController[i].gameObject.GetComponentInChildren<Animator>().Play("Empty");
                string newLetter = letter.ToString();
                plateController[i].gameObject.GetComponentInChildren<TextMesh>().text = newLetter;
                i++;
                yield return new WaitForSeconds(nextTiming);
            }
            break;
        }
    }

    IEnumerator Refresh()
    {
        for (int i = 0; i < plateController.Length; i++)
        {
            foreach (char letter in character)
            {
                if(plateController[i] != plateController[0])
                {
                    plateController[i].gameObject.GetComponentInChildren<Animator>().Play("Finding");
                }

                string newLetter = letter.ToString();
                plateController[i].gameObject.GetComponentInChildren<TextMesh>().text = "";
                i++;
                yield return new WaitForSeconds(0);
            }          
        }
    }
}
