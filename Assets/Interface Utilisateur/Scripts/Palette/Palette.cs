﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Palette : MonoBehaviour
{
    public GameObject palette;
    public string allCharact = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'";

    public void Refresh()
    {
        if (transform.childCount > 0)
        {
            Destroy(transform.GetChild(0).gameObject);
        }
    }

    public IEnumerator RotatePalette(char charact)
    {
        if(charact != ' ')
        {
            foreach (var item in allCharact)
            {
                GameObject go = Instantiate(palette, transform);
                go.GetComponentInChildren<Text>(go).text = item.ToString();

                float t = 0;
                while (t < 1)
                {
                    go.transform.localEulerAngles = new Vector3(Mathf.Lerp(0, -180, t), 0, 0);
                    t += Time.deltaTime * 12f;          
                    yield return null;
                    
                }
                if (item == charact)
                {
                    Debug.Log(item);
                    Debug.Log("found !");
                    go.transform.localEulerAngles = new Vector3(180, 0, 0);                  
                    break;               
                }
                else
                {
                    if (transform.GetChild(0) != null)
                    {
                        Destroy(transform.GetChild(0).gameObject);
                    }
                }
            }           
        }
    }
}
