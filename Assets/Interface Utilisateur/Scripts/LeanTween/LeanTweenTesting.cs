﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeanTweenTesting : MonoBehaviour
{
    public void BtnActive()
    {
        LeanTween.alpha(gameObject.GetComponent<RectTransform>(), 0, 1);
        LeanTween.scale(gameObject, new Vector3(0.5f, 0.5f, 0.5f), 1).setOnComplete(DeleteUI);
    }

    void DeleteUI()
    {
        Destroy(gameObject);
    }


}
