﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Notes : MonoBehaviour
{
    [Header("Notes pour le projet")]
    [SerializeField]
    [TextArea(1, 300)]
    private string notes;

}
