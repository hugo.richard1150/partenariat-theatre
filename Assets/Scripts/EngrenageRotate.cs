﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EngrenageRotate : MonoBehaviour
{
    private float speed;

    void Start()
    {
        speed = Random.Range(-40, 40);
    }

    void Update()
    {
        transform.Rotate(speed * Time.deltaTime, 0, 0);
    }
}
