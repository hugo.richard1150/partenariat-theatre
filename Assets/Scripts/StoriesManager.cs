﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StoriesManager : MonoBehaviour
{
    public GameObject[] stories;
    public GameObject[] storiesTitles;

    [HideInInspector] public int storyIndexReference;

    [HideInInspector] public Animator anim;

    public GameObject leftCurtain;
    public GameObject rightCurtain;

    public enum CurtainsStatut { Open, Close }
    public CurtainsStatut curtainsStatut;

    public bool coroutineCanBeCalled;

    private void Start()
    {
        anim = GetComponent<Animator>();
        coroutineCanBeCalled = true;
    }

    //Fonction appelée par les boutons
    public void LoadStory(int storyIndex)
    {
        if (coroutineCanBeCalled)
        {
            StartCoroutine(StoriesManagerEnum(storyIndex));
        }
    }

    private IEnumerator StoriesManagerEnum(int storyID)
    {
        coroutineCanBeCalled = false;

        //Les rideaux déjà fermés
        if (curtainsStatut == CurtainsStatut.Close)
        {
            Debug.Log("OUVERTURE");
            UnloadStories();
            storyIndexReference = storyID;
            LoadStories();

            anim.SetBool("OpenCurtains", true);
            OpenCurtains();
            yield return new WaitForSeconds(rightCurtain.GetComponent<Animation>()["Scene"].length);

            curtainsStatut = CurtainsStatut.Open;
            coroutineCanBeCalled = true;
        }
        //Les rideaux déjà ouverts
        else if (curtainsStatut == CurtainsStatut.Open)
        {
            Debug.Log("FERMETURE");
            anim.SetBool("OpenCurtains", false);
            CloseCurtains();
            yield return new WaitForSeconds(rightCurtain.GetComponent<Animation>()["Scene_Reversed"].length / 2);

            UnloadStories();
            storyIndexReference = storyID;

            curtainsStatut = CurtainsStatut.Close;
            coroutineCanBeCalled = true;

            StartCoroutine(StoriesManagerEnum(storyIndexReference));
        }
    }

    private void OpenCurtains()
    {
        ///Gestion de l'animation des rideaux
        //Ouverture du rideau de gauche
        Animation leftCurtainAnim = leftCurtain.GetComponent<Animation>();
        leftCurtainAnim.Play("Scene");
        leftCurtainAnim.transform.parent.localRotation = Quaternion.Euler(90, 0, 0);

        //Ouverture du rideau de droite
        Animation rightCurtainAnim = rightCurtain.GetComponent<Animation>();
        rightCurtainAnim.Play("Scene");
        rightCurtainAnim.transform.parent.localRotation = Quaternion.Euler(90, 0, 0);
    }

    private void CloseCurtains()
    {
        ///Gestion de l'animation des rideaux
        //Fermeture du rideau de gauche
        Animation leftCurtainAnim = leftCurtain.GetComponent<Animation>();
        leftCurtainAnim["Scene_Reversed"].speed = 2f;
        leftCurtainAnim.Play("Scene_Reversed");
        leftCurtainAnim.transform.parent.localRotation = Quaternion.Euler(90, 0, 0);

        //Fermeture du rideau de droite
        Animation rightCurtainAnim = rightCurtain.GetComponent<Animation>();
        rightCurtainAnim["Scene_Reversed"].speed = 2f;
        rightCurtainAnim.Play("Scene_Reversed");
        rightCurtainAnim.transform.parent.localRotation = Quaternion.Euler(90, 0, 0);
    }

    private void LoadStories()
    {
        //Activation des élèments graphiques de l'histoires selectionnée
        storiesTitles[storyIndexReference - 1].SetActive(true);
        stories[storyIndexReference - 1].SetActive(true);

        //Gestion des animations de l'histoire selectionnée
        anim.SetInteger("CurrentStory", storyIndexReference);
        anim.SetBool("PlayStory", true);
        anim.SetBool("TitleOpen", true);
        anim.SetBool("LoopStoryMain", false);

        Debug.Log("Story " + storyIndexReference + " Load");
    }

    private void UnloadStories()
    {
        for (int i = 0; i < stories.Length; i++)
        {
            stories[i].SetActive(false);
        }

        for (int i = 0; i < storiesTitles.Length; i++)
        {
            storiesTitles[i].SetActive(false);
        }
    }

    //Fonctions appelées par des animations
    public void TitleOpen()
    {
        anim.SetBool("TitleOpen", false);
        anim.SetBool("OpenCurtains", true);
    }

    public void FinishedStory()
    {
        anim.SetBool("PlayStory", false);
    }

    public void MainStoryLoop()
    {
        anim.SetBool("LoopStoryMain", true);
    }
}
