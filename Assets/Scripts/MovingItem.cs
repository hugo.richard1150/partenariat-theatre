﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingItem : MonoBehaviour
{
    float timeCounter = 0;

    public bool isXorZ = false;

    public float xPos;
    public float zPos;

    public float speed;
    public float width;
    public float height;


    // Update is called once per frame
     void Update()
    {
        timeCounter += Time.deltaTime * speed;

        float y = Mathf.Sin(timeCounter) * height;

        if (isXorZ is false)
        {
            float z = Mathf.Cos(timeCounter) * width;
            transform.localPosition = new Vector3(xPos, y, z);
        }
        else
        {
            float x = Mathf.Cos(timeCounter) * width;
            transform.localPosition = new Vector3(x, y, zPos);
        }
       

        


        //LeanTween.moveLocalX(gameObject, 1, 1f).setEaseLinear().setLoopPingPong();
        //LeanTween.moveLocalY(gameObject, 1, 0.5f).setEaseInOutSine().setLoopPingPong();
    }
}
